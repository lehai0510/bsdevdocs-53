<?php

namespace Bss\QuickOrder\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DB\TransactionFactory;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Sales\Model\Order\ShipmentFactory;

class Invoice
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     *
     * @var CollectionFactory
     */
    protected $_invoiceCollectionFactory;

    /**
     *
     * @var InvoiceService
     */
    protected $_invoiceService;

    /**
     *
     * @var ShipmentFactory
     */
    protected $_shipmentFactory;

    /**
     * @var TransactionFactory
     */
    protected $_transactionFactory;

    /**
     * @var \Bss\QuickOrder\Helper\Data
     */
    protected $_helper;

    /**
     * Invoice constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param CollectionFactory $invoiceCollectionFactory
     * @param InvoiceService $invoiceService
     * @param ShipmentFactory $shipmentFactory
     * @param TransactionFactory $transactionFactory
     * @param \Bss\QuickOrder\Helper\Data $helper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CollectionFactory $invoiceCollectionFactory,
        InvoiceService $invoiceService,
        ShipmentFactory $shipmentFactory,
        TransactionFactory $transactionFactory,
        \Bss\QuickOrder\Helper\Data $helper
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_invoiceCollectionFactory = $invoiceCollectionFactory;
        $this->_invoiceService = $invoiceService;
        $this->_shipmentFactory = $shipmentFactory;
        $this->_transactionFactory = $transactionFactory;
        $this->_helper = $helper;
    }

    /**
     * @return \Magento\Sales\Api\Data\InvoiceInterface|\Magento\Sales\Model\Order\Invoice|null
     */
    public function createInvoice()
    {
        $order = $this->_helper->loadQuickOrder();
        if (!$order) {
            return null;
        }
        return $this->actionCreateInvoice($order);
    }

    /**
     * @param $order
     * @return \Magento\Sales\Api\Data\InvoiceInterface|\Magento\Sales\Model\Order\Invoice|null
     */
    public function actionCreateInvoice($order)
    {
        try {
            $invoices = $this->_invoiceCollectionFactory->create()
                ->addAttributeToFilter('order_id', array('eq' => $order->getId()));

            $invoices->getSelect()->limit(1);

            if ((int)$invoices->count() !== 0 || !$order->canInvoice()) {
                return null;
            }

            $invoice = $this->_invoiceService->prepareInvoice($order);
            $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
            $invoice->register();
            $invoice->getOrder()->setCustomerNoteNotify(false);
            $invoice->getOrder()->setIsInProcess(true);
            $order->addStatusHistoryComment('Automatically INVOICED', false);
            $transactionSave = $this->_transactionFactory->create()->addObject($invoice)->addObject($invoice->getOrder());

            $transactionSave->save();
        } catch (\Exception $e) {
            $order->addStatusHistoryComment('Exception message: ' . $e->getMessage(), false);
            $order->save();
            return null;
        }

        return $invoice;
    }

}
