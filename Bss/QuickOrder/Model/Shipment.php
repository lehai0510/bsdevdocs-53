<?php

namespace Bss\QuickOrder\Model;

use Magento\Framework\Exception\LocalizedException;

class Shipment
{
    /**
     * @var \Bss\QuickOrder\Helper\Data|Order
     */
    protected $_helper;

    /**
     * @var \Magento\Sales\Model\Convert\Order
     */
    protected $_convertOrder;

    /**
     * @var \Magento\Shipping\Model\ShipmentNotifier
     */
    protected $_shipmentNotifier;

    /**
     * Shipment constructor.
     * @param \Bss\QuickOrder\Helper\Data $helper
     * @param \Magento\Sales\Model\Convert\Order $convertOrder
     * @param \Magento\Shipping\Model\ShipmentNotifier $shipmentNotifier
     */
    public function __construct(
        \Bss\QuickOrder\Helper\Data $helper,
        \Magento\Sales\Model\Convert\Order $convertOrder,
        \Magento\Shipping\Model\ShipmentNotifier $shipmentNotifier
    ) {
        $this->_helper = $helper;
        $this->_convertOrder = $convertOrder;
        $this->_shipmentNotifier = $shipmentNotifier;
    }

    /**
     * @return \Magento\Sales\Model\Order\Shipment|null
     * @throws LocalizedException
     */
    public function createShipment()
    {
        $order = $this->_helper->loadQuickOrder();
        if (!$order) {
            return null;
        }
        return $this->actionCreateShipment($order);
    }

    /**
     * @param $order
     * @return \Magento\Sales\Model\Order\Shipment
     * @throws LocalizedException
     */
    public function actionCreateShipment($order)
    {
        if (!$order->canShip()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('You can\'t create an shipment.')
            );
        }

        $shipment = $this->_convertOrder->toShipment($order);

        foreach ($order->getAllItems() as $orderItem) {
            // Check if order item has qty to ship or is virtual
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }
            $qtyShipped = $orderItem->getQtyToShip();
            try {
                $shipmentItem = $this->_convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
            } catch (LocalizedException $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($e->getMessage())
                );
            }
            $shipment->addItem($shipmentItem);
        }

        try {
            $shipment->register();
            $shipment->getOrder()->setIsInProcess(true);
            $shipment->save();
            $shipment->getOrder()->save();

            $this->_shipmentNotifier->notify($shipment);
            return $shipment->save();

        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }
    }

}
