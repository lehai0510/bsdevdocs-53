<?php

namespace Bss\QuickOrder\Model;

use Magento\Framework\Exception\LocalizedException;

class CreditMemo
{
    /**
     * @var \Bss\QuickOrder\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Sales\Model\Order\CreditmemoFactory
     */
    protected $_creditMemoFactory;

    /**
     * @var \Magento\Sales\Model\Order\Invoice
     */
    protected $_invoice;

    /**
     * @var \Magento\Sales\Model\Service\CreditmemoService
     */
    protected $_creditMemoService;
    
    /**
     * CreditMemo constructor.
     * @param \Bss\QuickOrder\Helper\Data $helper
     * @param \Magento\Sales\Model\Order\CreditmemoFactory $creditMemoFactory
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @param \Magento\Sales\Model\Service\CreditmemoService $creditMemoService
     */
    public function __construct(
        \Bss\QuickOrder\Helper\Data $helper,
        \Magento\Sales\Model\Order\CreditmemoFactory $creditMemoFactory,
        \Magento\Sales\Model\Order\Invoice $invoice,
        \Magento\Sales\Model\Service\CreditmemoService $creditMemoService
    ) {
        $this->_helper = $helper;
        $this->_creditMemoFactory = $creditMemoFactory;
        $this->_invoice = $invoice;
        $this->_creditMemoService = $creditMemoService;
    }

    /**
     * @return \Magento\Sales\Api\Data\CreditmemoInterface|null
     * @throws LocalizedException
     */
    public function createCreditMemo()
    {
        $order = $this->_helper->loadQuickOrder();
        if (!$order) {
            return null;
        }
        return $this->actionCreateCreditMemo($order);
    }

    /**
     * @param $order
     * @return \Magento\Sales\Api\Data\CreditmemoInterface|null
     * @throws LocalizedException
     */
    public function actionCreateCreditMemo($order)
    {
        $invoices = $order->getInvoiceCollection();
        foreach ($invoices as $invoice) {
            $invoice_id = $invoice->getIncrementId();
        }

        $invoiceObj = $this->_invoice->loadByIncrementId($invoice_id);
        $creditMemo = $this->_creditMemoFactory->createByOrder($order);

        $creditMemo->setInvoice($invoiceObj);

        try {
            return $this->_creditMemoService->refund($creditMemo);
        } catch (LocalizedException $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }
    }
}
