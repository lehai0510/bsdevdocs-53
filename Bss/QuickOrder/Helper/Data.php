<?php

namespace Bss\QuickOrder\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * Data constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param Context $context
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        Context $context
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_orderFactory = $orderFactory;
        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getOrderData()
    {
        $products = $this->getProductAdd();
        $shippingAddress = $this->getShippingAddress();

        if ($products && $shippingAddress) {
            return $tempOrder = [
                'currency_id' => 'USD',
                'email' => 'roni_cost@example.com', //buyer email id
                'shipping_address' => $shippingAddress,
                'items' => $products
            ];
        } else {
            return [];
        }
    }

    /**
     * @return array
     */
    public function getShippingAddress() {
        return [
            'firstname' => 'Veronica', //address Details
            'lastname' => 'Costello',
            'street' => '6146 Honey Bluff Parkway',
            'city' => 'Calder',
            'country_id' => 'US',
            'region' => 'xxx',
            'postcode' => '49628-7978',
            'telephone' => '(555) 229-3326',
            'fax' => '32423',
            'save_in_address_book' => 1
        ];
    }

    /**
     * @return array
     */
    public function getProductAdd()
    {
        $data = $this->_getRequest()->getParam('productId');

        if (empty($data)) {
            return [];
        } else {
            $items = [];
            foreach ($data as $item) {
                $items[] = [
                    'product_id' => $item,
                    'qty' => 1
                ];
            }
            return $items;
        }
    }

    /**
     * @return mixed
     */
    public function getQuickOrderId()
    {
        return $this->_checkoutSession->getQuickOrderId();
    }

    /**
     * @param $id
     * @return \Magento\Sales\Model\Order
     */
    public function getOrderById($id)
    {
        return $this->_orderFactory->create()->loadByIncrementId($id);
    }

    /**
     * @return \Magento\Sales\Model\Order
     */
    public function loadQuickOrder()
    {
        $idQuickOrder = $this->getQuickOrderId();
        return $this->getOrderById($idQuickOrder);
    }
}
