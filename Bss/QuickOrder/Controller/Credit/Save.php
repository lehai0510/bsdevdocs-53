<?php


namespace Bss\QuickOrder\Controller\Credit;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var \Bss\QuickOrder\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Bss\QuickOrder\Model\CreditMemo
     */
    protected $_creditMemo;

    /**
     * Save constructor.
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     * @param \Bss\QuickOrder\Helper\Data $helper
     * @param \Bss\QuickOrder\Model\CreditMemo $creditMemo
     * @param Context $context
     */
    public function __construct(
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        \Bss\QuickOrder\Helper\Data $helper,
        \Bss\QuickOrder\Model\CreditMemo $creditMemo,
        Context $context
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_helper = $helper;
        $this->_creditMemo = $creditMemo;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        if ($this->_creditMemo->createCreditMemo()) {
            $this->messageManager->addSuccessMessage(__('Credit Memo Success !!!'));
        } else {
            $this->messageManager->addErrorMessage(__('The Credit memo can\'t be saved at this time. Please try again later.'));
        }
        return $result;
    }
}
