<?php

namespace Bss\QuickOrder\Controller\Order;

use Bss\QuickOrder\Helper\Data;
use Bss\QuickOrder\Model\Order;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Order
     */
    protected $_order;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var Data
     */
    protected $_helper;

    /**
     * Save constructor.
     * @param Order $order
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     * @param Data $helper
     * @param Context $context
     */
    public function __construct(
        Order $order,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        Data $helper,
        Context $context
    ) {
        $this->_order = $order;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_helper = $helper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        try {
            if ($this->_order->createOrder()) {
                $data = $this->_helper->loadQuickOrder();
                $result->setData(['lastOrderId' => $data]);
                $this->messageManager->addSuccessMessage(__('Create Order Success !!!'));
            } else {
                $result->setData(['lastOrderId' => null]);
                $this->messageManager->addErrorMessage(__('The Order can\'t be saved at this time. Please try again later.'));
            }
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $result;
    }
}
