<?php

namespace Bss\QuickOrder\Controller\Shipment;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var \Bss\QuickOrder\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Bss\QuickOrder\Model\Invoice
     */
    protected $_shipment;

    /**
     * Save constructor.
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     * @param \Bss\QuickOrder\Helper\Data $helper
     * @param \Bss\QuickOrder\Model\Shipment $shipment
     * @param Context $context
     */
    public function __construct(
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        \Bss\QuickOrder\Helper\Data $helper,
        \Bss\QuickOrder\Model\Shipment $shipment,
        Context $context
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_helper = $helper;
        $this->_shipment = $shipment;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $shipment = $this->_shipment->createShipment();

        if ($shipment) {
            $this->messageManager->addSuccessMessage(__('Shipment Success !!!'));
        } else {
            $this->messageManager->addErrorMessage(__('The Shipment can\'t be saved at this time. Please try again later.'));
        }

        return $result;
    }
}
