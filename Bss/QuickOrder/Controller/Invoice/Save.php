<?php

namespace Bss\QuickOrder\Controller\Invoice;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var \Bss\QuickOrder\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Bss\QuickOrder\Model\Invoice
     */
    protected $_invoice;

    /**
     * Save constructor.
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     * @param \Bss\QuickOrder\Helper\Data $helper
     * @param \Bss\QuickOrder\Model\Invoice $invoice
     * @param Context $context
     */
    public function __construct(
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        \Bss\QuickOrder\Helper\Data $helper,
        \Bss\QuickOrder\Model\Invoice $invoice,
        Context $context
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_helper = $helper;
        $this->_invoice = $invoice;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();

        try {
            if ($this->_invoice->createInvoice()) {
                $this->messageManager->addSuccessMessage(__('Invoice Success !!!'));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __("The invoice can't be saved at this time. Please try again later.")
            );
            $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
        }

        return $result;
    }
}
