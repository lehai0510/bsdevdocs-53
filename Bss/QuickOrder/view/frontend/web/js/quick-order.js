define([
    "jquery",
    'jquery/ui'
], function ($) {
    'use strict';
    $.widget('mage.quickOrder', {
        _create: function () {
            $("#quickorder_selected_product option:checked").removeAttr("checked");
            this._actionCreateQuickOrder();
        },

        _actionCreateQuickOrder: function () {
            var self = this;
            // Create Order
            $("#quickorder_create_order").click(function () {
                var selectedProduct = $('#quickorder_selected_product:checked').map(function () {
                    return this.value;
                }).get();
                var url = self.options.orderUrl;

                $.ajax({
                    url: url,
                    type: "POST",
                    data: {productId: selectedProduct},
                    showLoader: true,
                    cache: false,
                    dataType: "json",
                    success: function (result) {
                        if (result.lastOrderId != null) {
                            self._showButtonInvoice();
                            self._showButtonShipment();
                            self._showButtonCreateNew();
                            self._hideButtonOrder();
                            self._hideOrderForm();
                            $(".quickorder-selected-product").prop("checked", false);
                        }
                    }
                });
            });

            // Create Invoice
            $("#quickorder_action_invoice").click(function () {
                var url = self.options.invoiceUrl;

                $.ajax({
                    url: url,
                    type: "POST",
                    showLoader: true,
                    cache: false,
                    success: function (result) {
                        self._hideButtonInvoice();
                        self._showButtonCredit();
                    }
                });
            });

            // Create Shipment
            $("#quickorder_action_shipment").click(function () {
                var url = self.options.shipmentUrl;

                $.ajax({
                    url: url,
                    type: "POST",
                    showLoader: true,
                    cache: false,
                    success: function (result) {
                        self._hideButtonShipment();
                    }
                });
            });

            // Create Credit Memo
            $("#quickorder_action_credit").click(function () {
                var url = self.options.creditUrl;

                $.ajax({
                    url: url,
                    type: "POST",
                    showLoader: true,
                    cache: false,
                    success: function (result) {
                        self._showButtonOrder();
                        self._showOrderForm();
                        self._hideButtonShipment();
                        self._hideButtonCredit();
                        self._hideButtonInvoice();
                        self._hideButtonCreateNew();
                    }
                });
            });

            // Create New Order
            $("#quickorder_create_new").click(function () {
                self._showButtonOrder();
                self._showOrderForm();
                self._hideButtonShipment();
                self._hideButtonCredit();
                self._hideButtonInvoice();
                self._hideButtonCreateNew();
            });
        },

        _showButtonInvoice: function () {
            $('#quickorder_action_invoice').show();
        },

        _showButtonOrder: function () {
            $('#quickorder_create_order').show();
        },

        _showButtonShipment: function () {
            $('#quickorder_action_shipment').show();
        },

        _showButtonCreateNew: function () {
            $('#quickorder_create_new').show();
        },

        _showButtonCredit: function () {
            $('#quickorder_action_credit').show();
        },

        _showOrderForm: function () {
            $('#quickorder_form').show();
        },

        _hideButtonOrder: function () {
            $('#quickorder_create_order').hide();
        },

        _hideOrderForm: function () {
            $('#quickorder_form').hide();
        },

        _hideButtonInvoice: function () {
            $('#quickorder_action_invoice').hide();
        },

        _hideButtonShipment: function () {
            $('#quickorder_action_shipment').hide();
        },

        _hideButtonCredit: function () {
            $('#quickorder_action_credit').hide();
        },

        _hideButtonCreateNew: function () {
            $('#quickorder_create_new').hide();
        },
    });
    return $.mage.quickOrder;
});
